#include  <msp430fg4618.h>
#include <intrinsics.h>// For BCD calculation
#include <stdint.h>
#include <stdlib.h>
#include  "LCD_defs.h"//P7_A1,P7_A2 ..so on are defined

int blink_count=0;
int blink;
int blink_val;
int blink_place;

int row;
int col;
int home;
struct al{
int al_hr;
int al_min;
int al_sec;
int box_num;
int days;
int num_pills;
int time_slot;
int days_left;
} *alarm;

struct al temp;


int max_index=-1;
int index=0;

int port_tgr=0;
int al_tgr=0;

void add_alarm(struct al var)
{
	max_index++;
	int i;
	alarm = (struct al *) realloc(alarm,(sizeof(struct al))*(max_index+1));
	if(max_index==0)
		alarm[0]=var;
	else
	{
	for(i=(max_index-1);i>=0;i--)
	{
		if(alarm[i].al_hr<var.al_hr)
		{
			alarm[i+1]=var;
			break;
		}
		else
		{
			if(alarm[i].al_hr>var.al_hr)
			{
				alarm[i+1]=alarm[i];
			}
			else
			{
				if(alarm[i].al_min<var.al_min)
							{
								alarm[i+1]=var;
								break;
							}
							else
							{
								if(alarm[i].al_min>var.al_min)
									alarm[i+1]=alarm[i];
								else
								{
									if(alarm[i].al_sec<var.al_sec)
									{
										alarm[i+1]=var;
										break;
									}
									else
									{
										if(alarm[i].al_sec>=var.al_sec)
											alarm[i+1]=alarm[i];
									}
								}
							}
			}
		}
	}
	if((alarm[0].al_hr==alarm[1].al_hr)&&(alarm[0].al_min==alarm[1].al_min)&&(alarm[0].al_sec==alarm[1].al_sec))
		alarm[0]=var;
	}
}

void delete_alarm(num)
{
	int i=0;
	if(num<max_index)
	{
		for(i=num;i<max_index;i++)
		{
			alarm[i]=alarm[i+1];
		}
	}
	max_index--;
	alarm = (struct al *) realloc(alarm,(sizeof(struct al))*(max_index+1));
}



display_LED(switch_box)
{
	P6OUT&=0x0F;
	switch(switch_box)
	{
			case 0x00: P6OUT|=0xE0;
			break;
			case 0x01: P6OUT|=0xC0;
			break;
			case 0x02: P6OUT|=0xA0;
			break;
			case 0x03: P6OUT|=0x80;
			break;
			case 0x04: P6OUT|=0x60;
			break;
			case 0x05: P6OUT|=0x40;
			break;
			case 0x06: P6OUT|=0x20;
			break;
			case 0x07: P6OUT|=0x00;
			break;
	}
}


int get_box()
{
	switch(P7IN & 0x31){
	case 0x31: return 0x01;
	case 0x30: return 0x02;
	case 0x21: return 0x03;
	case 0x20: return 0x04;
	case 0x11: return 0x05;
	case 0x10: return 0x06;
	case 0x01: return 0x07;
	case 0x00: return 0x08;
	default: return 0x00;
}
}



void display_alarm(num)
{
	LCD_all_off();
	LCD_Ahours(alarm[num].al_hr);
	LCD_Aminutes(alarm[num].al_min);
//	LCD_Asecs(alarm[num].al_sec);
	//	      P3_DOT_ON;
		      P5_DOT_ON;
    LCD_display(2,alarm[num].box_num);
	//display_LED(alarm[num].box_num);
}

int num_keys(int rownum,int colnum)
{
int temp=0x10;
if((colnum==1)&&(rownum==1))
	temp= 0x01;
if((colnum==2)&&(rownum==1))
	temp= 0x02;
if((colnum==3)&&(rownum==1))
	temp= 0x03;
if((colnum==1)&&(rownum==2))
	temp= 0x04;
if((colnum==2)&&(rownum==2))
	temp= 0x05;
if((colnum==3)&&(rownum==2))
	temp= 0x06;
if((colnum==1)&&(rownum==3))
	temp= 0x07;
if((colnum==2)&&(rownum==3))
	temp= 0x08;
if((colnum==3)&&(rownum==3))
	temp= 0x09;
if((colnum==2)&&(rownum==4))
	temp= 0x00;

	return temp;
}


void set_alarm()
{
	blink=1;
	blink_val=0x00;
	home=0;
	struct al set;

	int dosage_per_day=0;
	int j;
	int digit;
	LCD_all_off();
	blink_place=0x02;
	__bis_SR_register(LPM3_bits + GIE);
	digit=num_keys(row,col);
	set.box_num=digit; //Box No input.
	row=0;col=0;
	blink=0;
	LCD_display(2,set.box_num);

	for(;;){
	__bis_SR_register(LPM3_bits + GIE);
	if((row==4)&&(col==4))
	break;
	}

	blink=1;
	LCD_all_off();
	LCD_display(3,0x00);
	LCD_display(2,0x00);
	blink_place=0x03;
	__bis_SR_register(LPM3_bits + GIE);
	digit=num_keys(row,col);

	blink_place=0x02;
	LCD_display(3,digit);
	set.days=0x10*(digit); //No of days, 1st Digit input.
	row=0;col=0;
	__bis_SR_register(LPM3_bits + GIE);
	digit=num_keys(row,col);
	blink=0x00;
	LCD_display(2,digit);
	set.days+=digit; //No of days, 2nd Digit input.
	row=0;col=0;
	set.days_left=set.days;
	//LCD_display(); //DISPLAY ENTERED NO. OF DAYS
	for(;;){
	__bis_SR_register(LPM3_bits + GIE);
	if((row==4)&&(col==4))
	break;
	}
	blink=1;
	LCD_all_off();
	LCD_display(3,0x00);
	LCD_display(2,0x00);
	blink_place=0x03;
	__bis_SR_register(LPM3_bits + GIE);
	digit=num_keys(row,col);
	blink_place=0x02;
	LCD_display(3,digit);
	set.num_pills=0x10*(digit); //No of pills, 1st Digit input.
	row=0;col=0;
	__bis_SR_register(LPM3_bits + GIE);
	digit=num_keys(row,col);
	blink=0;
	LCD_display(2,digit);
	set.num_pills+=digit; //No of pills, 2nd Digit input.
	row=0;col=0;
	//LCD_display(); //DISPLAY ENTERED NO. OF PILLS
	for(;;){
	__bis_SR_register(LPM3_bits + GIE);
	if((row==4)&&(col==4))
	break;
	}
	blink=1;
	LCD_all_off();
	LCD_display(3,0x00);
	LCD_display(2,0x00);
	blink_place=0x03;
	__bis_SR_register(LPM3_bits + GIE);
	digit=num_keys(row,col);

	blink_place=0x02;
	LCD_display(3,digit);
	dosage_per_day=0x10*(digit); //No of dosage_per_day, 1st Digit input.
	row=0;col=0;
	__bis_SR_register(LPM3_bits + GIE);
	digit=num_keys(row,col);
	blink=0;
	LCD_display(2,digit);
	dosage_per_day+=digit; //No of dosage_per_day, 2nd Digit input.
	row=0;col=0;
	//LCD_display(); //DISPLAY ENTERED NO. OF dosage_per_day
	for(;;){
	__bis_SR_register(LPM3_bits + GIE);
	if((row==4)&&(col==4))
	break;
	}//Enter alarms, one by one
	for(j=0;j<dosage_per_day;j++)
	{
		//Enter Alarm(j+1)
	//P2_A0;P3_A0;P4_A0;P5_A0;P6_A0;P7_A0;P3_DOT_ON;P5_DOT_ON;
	LCD_all_off();
	P2_A0;
	P3_A0;
	P4_A0;
	P5_A0;
	P6_A0;
	P7_A0;
	P3_DOT_ON;
	P5_DOT_ON;
	blink=1;
	blink_place=0x07;
	__bis_SR_register(LPM3_bits + GIE);
	blink_place=0x06;
	digit=num_keys(row,col);
	row=0;col=0;
	set.al_hr=(0x10)*(digit);
	LCD_Ahours(set.al_hr);
	__bis_SR_register(LPM3_bits + GIE);
	blink_place=0x05;
	digit=num_keys(row,col);
	row=0;col=0;
	set.al_hr+=digit;
	set.time_slot=set.al_hr;
	LCD_Ahours(set.al_hr);
	P3_DOT_ON;
	P5_DOT_ON;
	__bis_SR_register(LPM3_bits + GIE);
	blink_place=0x04;
	digit=num_keys(row,col);
	row=0;col=0;
	set.al_min=(0x10)*(digit);
	LCD_Aminutes(set.al_min);
	P3_DOT_ON;
	P5_DOT_ON;
	__bis_SR_register(LPM3_bits + GIE);
	blink_place=0x03;
	digit=num_keys(row,col);
	row=0;col=0;
	set.al_min+=digit;
	LCD_Aminutes(set.al_min);
	P3_DOT_ON;
	P5_DOT_ON;
	__bis_SR_register(LPM3_bits + GIE);
	blink_place=0x02;
	digit=num_keys(row,col);
	row=0;col=0;
	set.al_sec=(0x10)*(digit);
	LCD_Asecs(set.al_sec);
	P3_DOT_ON;
	P5_DOT_ON;
	__bis_SR_register(LPM3_bits + GIE);blink=0;
	digit=num_keys(row,col);
	row=0;col=0;
	set.al_sec+=digit;
	LCD_Asecs(set.al_sec);
	P3_DOT_ON;
	P5_DOT_ON;
		for(;;)
		{
		__bis_SR_register(LPM3_bits + GIE);
		if((row==4)&&(col==4)){
		add_alarm(set);
		row=0;col=0;
		break;
		}
		}
	}
home=1;
row=0;
col=0;
}



void list_alarm()
{
   int num=0;
   home=0;
   display_alarm(num);
  __bis_SR_register(LPM3_bits + GIE);
	  for(;;)
	  {

	    if((col==1)&&(row==4))
		{

		     display_LED(0);
	    	home=1;
		   break;
		}
		if((col==4)&&(row==3))
		{
		    delete_alarm(num);
			if(num == (max_index+1))
				num--;
			display_alarm(num);
			__bis_SR_register(LPM3_bits + GIE);
		}

	    if((col==4)&&(row==1))
		{
		   if(num==0)
		   	   	   {
		   		    	display_alarm(num);
		   		    	__bis_SR_register(LPM3_bits + GIE);
		   		    }
		   else
		   {
			   num--;
			   display_alarm(num);
	           __bis_SR_register(LPM3_bits + GIE);
		   }
		 }
	     if((col==4)&&(row==2))
	     {
		    if(num==max_index)
		    {
		    	display_alarm(num);
		    	__bis_SR_register(LPM3_bits + GIE);
		    }
		    else
			{
	        num++;
		    display_alarm(num);
	        __bis_SR_register(LPM3_bits + GIE);
			}
	     }

      }
}

#pragma vector=BASICTIMER_VECTOR
__interrupt void basic_timer_ISR(void)
{
	if(alarm[index].time_slot==RTCHOUR)	//Modify this in accordance with the slots
		al_tgr=1;
	else
		{
		display_LED(0x00);
		al_tgr=0;
		if(RTCHOUR!=0x23)
		{
			while((alarm[index].time_slot<RTCHOUR)&&(index<max_index))
				index++;

			}
		}
	if(RTCHOUR==0x23 && RTCMIN==0x59 && RTCSEC==50)
		index=0;

	if(al_tgr==1)
	{
    	display_LED(alarm[index].box_num);
		if((RTCMIN==0x00)||(RTCMIN==0x01)||(RTCMIN==0x02)||(RTCMIN==0x03))	//After every 15 minutes
		{
		if(RTCSEC<=0x15)
			P3OUT|=0x20;
		else
			P3OUT&=~0x20;
		}
	}
	if(al_tgr==0)
	{
		P3OUT&=~0x20;
	}

	if(home==1)
		{
		LCD_all_off();
		LCD_hours();
		LCD_minutes();
		LCD_seconds();
		if (RTCSEC & 0x01)			    // toogle clock dots
			    {
			      P3_DOT_ON;
			      P5_DOT_ON;
			    }
			 else
			    {
			      P3_DOT_OFF;
			      P5_DOT_OFF;
			    }
		}
	if(blink==1)
	{
		if(blink_count==0)
		{
			blink_count=1;
			LCD_display(blink_place,blink_val);
		}
		else
		{
			blink_count=0;
			LCD_display(blink_place,0x10);
		}
	}
}

#pragma vector=PORT2_VECTOR
__interrupt void PORT2_ISR(void)
{
	volatile unsigned int i;
			  for (i=0x1FFF; i > 0; i--);//to tackle switch bounce

	P5OUT ^= 0x02;
	col=0;
	row=0;
    switch(P2IFG)
	{
	case 0x01:
			col=1;
		     R1_ON;
		     if((P2IN & 0x01)==0x01)
		    	 row=1;
		     else {
		           R2_ON;
		           if((P2IN & 0x01)==0x01)
		        	   row=2;
		           else {
		                R3_ON;
		                if((P2IN & 0x01)==0x01)
		                	row=3;
		                else
		                	row=4;
		           	   	 }
		     	 }
		     break;

	case 0x02:
			col=2;
		     R1_ON;
		     if((P2IN & 0x02)==0x02)
		    	 row=1;
		     else {
		           R2_ON;
		           if((P2IN & 0x02)==0x02)
		        	   row=2;
		           else {
		                R3_ON;
		                if((P2IN & 0x02)==0x02)
		                	row=3;
		                else
		                	row=4;
		           	   	 }
		     	 }
		     break;
	case 0x04:
			col=3;
		     R1_ON;
		     if((P2IN & 0x04)==0x04)
		    	 row=1;
		     else {
		           R2_ON;
		           if((P2IN & 0x04)==0x04)
		        	   row=2;
		           else {
		                R3_ON;
		                if((P2IN & 0x04)==0x04)
		                	row=3;
		                else
		                	row=4;
		           	   	 }
		     	 }
		     break;
	case 0x40:
			col=4;
		     R1_ON;
		     if((P2IN & 0x40)==0x40)
		    	 row=1;
		     else {
		           R2_ON;
		           if((P2IN & 0x40)==0x40)
		        	   row=2;
		           else {
		                R3_ON;
		                if((P2IN & 0x40)==0x40)
		                	row=3;
		                else
		                	row=4;
		           	   	 }
		     	 }
		     break;
	case 0x80:
		home=0;
		LCD_all_off();
		LCD_display(3,get_box());
		if(alarm[index].box_num ==get_box())
		{
			index++;
			al_tgr=0;
		}
		 P7OUT&=~0XFF;
		 int i1;
		 for(i1=0;i1<30500;i1++)
		 home=1;
		break;

	}
    R1_OFF;
    R2_OFF;
    R3_OFF;
    R4_OFF;
    P2IFG &= ~0xC7;
    _BIC_SR(LPM3_EXIT);
}


void main(void)
{
 WDTCTL = WDTPW | WDTHOLD;              // Stop WDT
 FLL_CTL0 |= XCAP18PF;                  // Set load cap for 32k xtal
 P5DIR |= 0x1D;                           // Ports P5.2, P5.3 and P5.4 as outputs
 P5SEL |= 0x1D;                           // Ports P5.2, P5.3 and P5.4 as special function (COM1, COM2 and COM3)

 // LCD_A S0-S21 configuration
 LCDAPCTL0 = LCDS24 | LCDS20 | LCDS16 | LCDS12 | LCDS8 | LCDS4;

 // LCD_A configuration
 LCDACTL = LCDFREQ_192 | LCD4MUX | LCDSON | LCDON;   // (ACLK = 32768)/192, 4-mux LCD, LCD_A on, Segments on
 LCDAVCTL0 = LCDCPEN;                                // Charge pump enable
 LCDAVCTL1 = VLCD_3_02;                              // VLCD = 3,44 V

 // clean LCD
 LCD_all_off();

 // RTC configuration
 RTCCTL = RTCBCD | RTCHOLD | RTCMODE_3;
                                        // BCD mode, RTC e BT disable
                                        // Interrupt disable,
// Init alarm
//Init Clock
  RTCSEC =  0x50;                       // Set Seconds in BCD format
  RTCMIN =  0x59;                       // Set Minutes in BCD format
  RTCHOUR = 0x23;                       // Set Hours in BCD format
  RTCDOW =  0x01;                       // Set Day-of-Week  in BCD format
  RTCDAY =  0x05;                       // Set Day in BCD format
  RTCMON =  0x01;                       // Set Month in BCD format
  RTCYEAR = 0x2014;                     // Set Year in BCD format

  RTCCTL &= ~RTCHOLD;                   // Enable RTC
  row=0;
  col=0;
  home=1;

  // Basic Timer 1 Configuration
  BTCTL = BT_fCLK2_DIV32;               // (ACLK/256)/64
  IE2 |= BTIE;                          // Enable BT interrupt with 0.5 period
  // LCD init

 P2SEL &= ~0xC7;                       // P1.0 and P1.1 I/O ports
 P2DIR &= ~0xC7;                       // P1.0 and P1.1 digital inputs  //PORT1 CONFIGURATION
 P2IFG &= ~0xC7;                        // Clear P1 flags
 P2IES |= 0x47;                       // high-to-low transition interrupts
 P2IES &= 0x7F;
 //P2IES &= ~0x80;
 P2IE |= 0xC7;                         // enable port interrupts
 P6DIR|=0xEF;
 P3DIR|=0x20;
 P7DIR|=0X00;
 P7OUT&=~0XFF;
 P3OUT&=~0x20;
 display_LED(0x00);
 R1_OFF;
 R2_OFF;
 R3_OFF;
 R4_OFF;
 P5DIR |= 0x02;
 P5OUT &= 0xFD;
 temp.al_hr=0x00;
 temp.al_min=0x01;
 temp.num_pills=0x05;
 temp.box_num=0x03;
 temp.time_slot=0x00;
 add_alarm(temp);
 temp.al_hr=0x01;
 temp.al_min=0x20;
 temp.num_pills=0x05;
 temp.box_num=0x04;
 temp.time_slot=0x01;
 add_alarm(temp);

 for(;;)
 	  {
	 	if((col==4)&&(row==2))
	 		{
	 		row=0;col=0;
	 		list_alarm();
			__bis_SR_register(LPM3_bits + GIE);
	 		}
	 	if((col==4)&&(row==1))
	 		{
	 	    row=0; col=0;
	 	    set_alarm();
			__bis_SR_register(LPM3_bits + GIE);
	 		}
 	  __bis_SR_register(LPM3_bits + GIE);
 	  }
}
