# README #

### Quick Summary ###

* The codebase for MedAssist, designed and developed as a part of TIIC IADC 2014.
* 1.0.0
* http://bit.ly/med_assist

### Team Contact ###

* Kshitiz Gupta (g.kshitiz93@gmail.com)
* Arpit Jain (arpitjain1821@gmail.com)
* Harsha Vardhan (pokallaharsha@gmail.com)
* Sumit Singh (sumeetsinghof1993@gmail.com)
* Aashish Amber (aashishamber@gmail.com)