#include <SPI.h>
#include <AIR430BoostETSI.h>

// -----------------------------------------------------------------------------
/**
 *  Defines, enumerations, and structure definitions
 */

#define CMD_OFF         '0'
#define CMD_ON          '1'

unsigned char txData[3] = {'0', '0', '\0'};  //transmitter pack containing 2 bits
                                             // {trigger, tx}
unsigned char rxData[2] = {'0','\0'};        //reciever pack containing 1 bit - rx

// -----------------------------------------------------------------------------
// Debug print functions

void printTxData()
{
  Serial.print("TX (DATA): ");
  Serial.println((char*)txData); 
}
void printRxData()
{
  Serial.print("RX (TGRP_trans,tx_trans): ");
  Serial.println((char*)rxData);
}

// Variables will change:
int triggerState = 1;         // current state of the button
// -----------------------------------------------------------------------------
// Main example

void setup()
{
  // Setup initial address, channel, and TX power.
  Radio.begin(0x01, CHANNEL_1, POWER_MAX);

  // Setup serial for debug printing.
  Serial.begin(9600);
}

void loop()
{
  Serial.println("Transmitter");  //Continous printing for identification
  
  // Read the trigger, i.e., PIN6 of the IC (in this case)
  triggerState = digitalRead(6);
  
  //Definition of all the conditions and corresponding transmission.

  //Condition 1.
  //Trigger OFF, Reciever_tx OFF
  //This implies, all OFF state
  if(triggerState==0&&rxData[0]==CMD_OFF){
    txData[0]=CMD_OFF;
    txData[1]=CMD_OFF;
  }

  //Condition 2.
  //Trigger ON, Reciever_tx OFF
  //This implies, ring remote Alarm, i.e, Transmit {1,1}  
  if(triggerState==1&&rxData[0]==CMD_OFF){
    txData[0]=CMD_ON;
    txData[1]=CMD_ON;
  }

  //Condition 3.
  //Trigger ON, Reciever_tx ON
  //This implies, User pressed button. Turn off remote Alarm, i.e, Transmit {1,0}
  if(triggerState==1&&rxData[0]==CMD_ON){
    txData[0]=CMD_ON;
    txData[1]=CMD_OFF;
  }

  //Condition 4.
  //Trigger OFF, Reciever_tx ON
  //This implies, Trigger went off. Reset remote transmission to all OFF
  //i.e, Transmit {0,0}  
  if(triggerState==0&&rxData[0]==CMD_ON){
    txData[0]=CMD_OFF;
    txData[1]=CMD_OFF;
  }
  
  Radio.transmit(ADDRESS_BROADCAST, txData, 3);  //Transmit data
  printTxData();         //Print transmitted packet, debugging  
  
  while (Radio.busy());
  
  // Turn on the receiver and listen for incoming data. Timeout after 1 seconds.
  // The receiverOn() method returns the number of bytes copied to rxData.
  Radio.receiverOn(rxData, sizeof(rxData), 1000);
  delay(500);
}

