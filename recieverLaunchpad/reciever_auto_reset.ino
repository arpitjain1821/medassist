#include <SPI.h>
#include <AIR430BoostETSI.h>

// -----------------------------------------------------------------------------

//Defines, enumerations, and structure definitions

#define CMD_OFF         '0'
#define CMD_ON          '1'

// -----------------------------------------------------------------------------
/**
 *  Global data
 */

unsigned char txData[2] = {'0','\0'};        //transmitter pack containing 1 bit - tx
unsigned char rxData[3] = {'0', '0', '\0'};  //reciever pack containing 2 bits
                                             //{transmitted_trigger, transmitted_tx}

// -----------------------------------------------------------------------------
// Debug print functions

void printTxData()
{
  Serial.print("TX (DATA): ");
  Serial.println((char*)txData); 
}
void printRxData()
{
  Serial.print("RX (TGRP_trans,tx_trans): ");
  Serial.println((char*)rxData);
}

// Variables will change:
int buttonState = 1;         // current state of the button
int lastButtonState = 1;     // previous state of the button

// -----------------------------------------------------------------------------
// Main code

void setup()
{
  // setup initial address, channel, and TX power.
  Radio.begin(0x01, CHANNEL_1, POWER_MAX);

  // Setup serial for debug printing.
  Serial.begin(9600);
  
  //  Setup LED for example demonstration purposes.
  pinMode(RED_LED, OUTPUT);
  digitalWrite(RED_LED, LOW);
  
  // Setup push button.
  pinMode(PUSH2, INPUT_PULLUP);
}

void loop()
{
  Serial.println("reciever");  //Continous printing for identification
  
  // read the pushbutton input pin:
  buttonState = digitalRead(PUSH2);
  
  // compare the buttonState to its previous state
  if (buttonState != lastButtonState)
  {
      if(buttonState==0) // 1 TO 0 transition, desired transition
      {
        if(rxData[0]==CMD_ON&&rxData[1]==CMD_ON){  //If transition occurs with transmitter_trigger = ON
                                                   //and transmitter_tx = ON, then sent tx = ON
               txData[0]=CMD_ON;                   //Identification of toggle when alarm is on
        }
     }
  }
  lastButtonState = buttonState;
  
  if(rxData[0]==CMD_OFF){       //If transition occurs with transmitter_trigger = OFF
                                //and transmitter_tx = OFF, then sent tx = OFF
      txData[0]=CMD_OFF;        //All OFF stage.
    }
  
  Radio.transmit(ADDRESS_BROADCAST, txData, 1);  //Transmit data
  printTxData(); //Print transmitted packet, debugging

  while (Radio.busy());
 
  // Turn on the receiver and listen for incoming data. Timeout after 1 seconds.
  // The receiverOn() method returns the number of bytes copied to rxData.
  if ((Radio.receiverOn(rxData, sizeof(rxData), 1000)) > 0)
  {
    // Perform action based on transmitter_tx: turn on/off red LED.
    if (rxData[1] == CMD_ON)  //Transmitter_tx = ON, turn ON LED
    {
      digitalWrite(RED_LED, HIGH);
    }
    else   //Transmitter_tx = OFF, turn OFF LED
    {
      digitalWrite(RED_LED, LOW);
    }
  }

   printRxData();  //Print recieved packet, debugging
  delay(500);
}
